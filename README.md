[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

This project is not maintained in any way. Feel free to clone and modify to your own likes, but expect no work on it from my side.

# SmsSender

SmsSender is a JSON-RPC based SMS Sender, which can interact with
gergely/smsgateway.

## Features

* Send SMS messages through [gergely/smsgateway](/gergely/smsgateway)
